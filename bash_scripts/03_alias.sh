#!/bin/bash

#tput utils
export bold=$(tput bold)
export underline=$(tput sgr 0 1)
export reset=$(tput sgr0)

export purple=$(tput setaf $TPUT_MAGENTA)
export magenta=$(tput setaf $TPUT_MAGENTA)
export red=$(tput setaf $TPUT_RED)
export green=$(tput setaf $TPUT_GREEN)
export tan=$(tput setaf $TPUT_YELLOW)
export blue=$(tput setaf $TPUT_BLUE)

function tput_reset(){
    if [ ! -e $HOME/.til_bcolor ]; then
        tput sgr0
    fi
}

function tput_af(){
    if [ ! -e $HOME/.til_bcolor ]; then
        tput setaf $1
    fi
}

#Basic substitutions
alias cls='clear'
alias p='ps aux | grep '
alias nanomirlist='sudo nano /etc/pacman.d/mirrorlist'
alias nanofstab='sudo nano /etc/fstab'
alias c+x='chmod +x -v'
alias b-dl='bandcamp-dl'
alias ytdl='youtube-dl --socket-timeout 2'
alias ytdl360="ytdl -f 18"

if hash hub 2>/dev/null; then
    alias git='hub'
fi

if hash pydf 2>/dev/null; then
    alias df='pydf'
fi

alias bd=". bd -si"
alias ll="ls -la"
alias sys-vitalbackup="sudo bash $HOME/til/bash_utils/sys-vitalbackup.sh"
alias sysctl-susp="sudo systemctl suspend"
alias sysctl-hibr="sudo systemctl hibernate"
alias sanity_test="sudo $HOME/til/bash_utils/sanity.py"

# Network aliases
alias pingg='ping google.com'
alias mac='ip link | grep "link/ether"'

# pacman aliases
alias pcmi='sudo pacmatic -S'
alias pcmu='sudo pacmatic -Syu'

# show $PATH
alias path='echo -e ${PATH//:/\\n}'

# root
alias god='sudo -i'
alias root='sudo -i'

# forever running train
alias foreversl='while :; do sl; done'

# git aliases
alias gtst='git status'
alias gaa='git add -A'
alias gct='git commit'
alias gcta='git commit -a'
alias gpsh='git push'
alias gpll='git pull'
alias gact='git add -A && git commit'
alias gpllad='find . -mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} pull origin master \;'
alias gpshad='find . -mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} push origin master \;'

# mlsh
alias mlsh='$HOME/lnmnds.github/mlsh/mlsh'

# mounting
#alias mount-windows='sudo udevil mount /dev/sda4'
#alias mount-debext='sudo udevil mount /dev/sda11'
#alias mount-debext2='sudo udevil mount /dev/sda7'
#alias mount-all='mount-debext && mount-steam && mount-windows'
