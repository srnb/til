#!/bin/bash

# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "[${BRANCH}${STAT}] "
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

function smiley() {
    echo -e ":\x$(($??28:29))"
}

function get_freemem(){
    echo -e "$(awk '/MemFree/{print $2=$2/1024}' /proc/meminfo)MB"
}

function make_color_from_hostname(){
	tput_af $(hostname | sum | awk -v ncolors=$(infocmp -1 | expand | sed -n -e "s/^ *colors#\([0-9][0-9]*\),.*/\1/p") 'ncolors>1 {print 1 + ($1 % (ncolors - 1))}')
	echo "$HOSTNAME"
}

function make_color_from_user(){
	USR=$(whoami)
	tput_af $(echo $USR | sum | awk -v ncolors=$(infocmp -1 | expand | sed -n -e "s/^ *colors#\([0-9][0-9]*\),.*/\1/p") 'ncolors>1 {print 1 + ($1 % (ncolors - 1))}')
	echo "$USR"
}

export PS1=""
export PS1+="\[$(tput_af $TPUT_BLUE)\]"
export PS1+="$USER "
export PS1+="\`hostname\` "
export PS1+="\[$(tput bold)\]"
export PS1+="\[$(tput_af $TPUT_MAGENTA)\]"
export PS1+="\w "
export PS1+="\[$(tput_af $TPUT_WHITE)\]"
# export PS1+=" \`parse_git_branch\`"
if [ ! -e $HOME/.til_laggy ]; then # PS1 without the freemem method and time
    export PS1+="\[$(tput_af $TPUT_CYAN)\]"
    export PS1+="[\t] "
    export PS1+="\[$(tput_af $TPUT_RED)\]"
    export PS1+="\`get_freemem\`"
fi
export PS1+="\n\[$(tput_af $TPUT_WHITE && tput_reset)\]\$ "

export PS1
