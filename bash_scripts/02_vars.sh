#!/bin/bash
export VISUAL="nvim"
export EDITOR="nvim"

#enviroment vars
export MAKEFLAGS="-j`nproc`"

export TPUT_BLACK=0
export TPUT_RED=1
export TPUT_GREEN=2
export TPUT_YELLOW=3
export TPUT_BLUE=4
export TPUT_MAGENTA=5
export TPUT_CYAN=6
export TPUT_WHITE=7

export USE_CCACHE=1
