#!/bin/bash

[[ $- != *i* ]] && return

# load configuration file, if any
[[ -s ~/.til_config ]] && source ~/.til_config

# make CTRL + R usable etc
stty -ixon

# Complete after sudo
complete -cf sudo

shopt -s checkwinsize
shopt -s globstar

# enable zsh like auto nd 'set show-all-if-ambiguous on'
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'

#history
shopt -s histappend
export HISTSIZE=1000
export HISTFILESIZE=1000
HISTCONTROL=ignoredups

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"

    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# do some handy aliases(actually the program does for me /shrug)
if hash thefuck 2>/dev/null; then
    eval $(thefuck --alias)
fi

if hash hub 2>/dev/null; then
    eval $(hub alias -s)
fi

# set X11 keyboard map when available
if type "setxkbmap" > /dev/null; then
    if [ "$KBDLAYOUT" != "" ]; then
        setxkbmap -layout "$KBDLAYOUT"
    fi
fi

# base16 shell
BASE16_SHELL="$HOME/.config/base16-shell/scripts/base16-monokai.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
