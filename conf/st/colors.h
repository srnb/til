const char *colorname[] = {

  [0] = "#373348",
  [1] = "#f92672",
  [2] = "#c2ffdf",
  [3] = "#e6c000",
  [4] = "#8077a8",
  [5] = "#716799",
  [6] = "#ae81ff",
  [7] = "#f8f8f0",

  [8]  = "#a8a4b1",
  [9]  = "#ff857f",
  [10] = "#c2ffdf",
  [11] = "#fff352",
  [12] = "#ae81ff",
  [13] = "#c5a3ff",
  [14] = "#c5a3ff",
  [15] = "#ffffff",

  [256] = "#5a5475",
  [257] = "#f8f8f0",
  [258] = "#ffffff",
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
