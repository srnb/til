#!/bin/bash

# general files
ln -s $HOME/til/init.fish $HOME/.config/fish/config.fish
ln -s "$HOME/til/fish/functions" "$HOME/.config/fish/functions"

ln -s $HOME/til/awesome $HOME/.config

# bash scripts are hellish rn, just disable.
## ln -s $HOME/til/bashrc $HOME/.bashrc

ln -s $HOME/til/X11/Xresources $HOME/.Xresources

# vim!!!
mkdir -p ~/.config/nvim
mkdir -p ~/.local/share/nvim
mkdir -p ~/.local/share/nvim/plugged
ln -s $HOME/til/vimrc $HOME/.vimrc
ln -s $HOME/til/nvimrc $HOME/.config/nvim/init.vim

# emacs configuration
mkdir -p ~/.emacs
mkdir -p ~/.emacs.d
ln -s $HOME/til/emacs/.emacs $HOME/.emacs
ln -s $HOME/til/emacs/.emacs.d $HOME/.emacs.d

# config files
ln -s $HOME/til/conf/tmux.conf $HOME/.tmux.conf
ln -s $HOME/til/conf/redshift.conf $HOME/.config/redshift.conf

mkdir -p $HOME/.config/kitty
ln -s $HOME/til/conf/kitty.conf $HOME/.config/kitty/kitty.conf

# mpd
mkdir -p ~/.config/mpd
mkdir ~/.config/mpd/playlists
touch ~/.config/mpd/database
touch ~/.config/mpd/log

ln -s $HOME/til/conf/mpd.conf ~/.config/mpd/mpd.conf

# conky
mkdir -p ~/.config/conky
ln -s $HOME/til/conf/conky.conf $HOME/.config/conky/conky.conf

# qutebrowser userscripts
mkdir -p ~/.local/share/qutebrowser/userscripts
ln -s $HOME/til/qutebrowser/userscripts $HOME/.local/share/qutebrowser

# luakit config
ln -s $HOME/til/luakit $HOME/.config/luakit
