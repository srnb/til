set nocompatible              " be iMproved, required
set termguicolors

call plug#begin("~/.vim/plugged")

" simpler plugin set than nvim
Plug 'elixir-lang/vim-elixir'
Plug 'rhysd/committia.vim'

call plug#end()

set background=dark
source /home/luna/til/vim/base.vim

" Put all temporary files under the same directory.
" https://github.com/mhinz/vim-galore#handling-backup-swap-undo-and-viminfo-files
set backup
set backupdir   =$HOME/.vim/files/backup/
set backupext   =-vimbackup
set backupskip  =
set directory   =$HOME/.vim/files/swap/
set updatecount =100
set undofile
set undodir     =$HOME/.vim/files/undo/
set viminfo     ='100,n$HOME/.vim/files/info/viminfo

" colorscheme delek
set mouse=a
