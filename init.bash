#!/bin/bash
# init.bash - initialize packages and stuff for a new computer

echo "this script will install fortnite for you"
echo "run this as ./init.bash laptop if you wanna install fortnite on a lappy"

do_root(){
    sudo $@
}

init_package(){
    which pacman >/dev/null 2>&1
    if [ $? -eq 1 ]
    then
        # Void Linux
        PKG='xbps-install'
        do_root xbps-install -Suv
    else
        PKG='pacman'
        do_root pacman -Syu
        do_root pacman -Fy
    fi
}

do_pkg(){
    which pacman >/dev/null 2>&1
    if [ $? -eq 1 ]
    then
        do_root $PKG -Sy $@
    else
        do_root pacman -S --needed --noconfirm $@
    fi
}

do_pp(){
    which pacman >/dev/null 2>&1
    if [ $? -eq 1 ]
    then
        do_root $PKG -Sy $1
    else
        do_root pacman -S --needed --noconfirm $2
    fi
}

init_package

# do packages

# minimal build enviroment for Void Linux
do_pp gcc-c++ ""
do_pp make ""
do_pp texinfo ""
do_pp gettext ""
do_pp perl ""
do_pp flex ""
do_pp pkg-config ""

# enable nonfree
do_pp void-repo-nonfree ""
if [ "$(uname -m)" = "x86_64" ]; then
    do_pp void-repo-multilib ""
fi

# tools
do_pkg git lua go

# python 2.x (python on void, python2 on arch)
do_pp python python2
do_pp python-pip python2-pip

# python 3.x (python3 on void, python on arch)
do_pp python3 python
do_pp python3-pip python-pip

# Void doesn't provide libraries by default
do_pp python-devel ""
do_pp python3-devel ""

do_root pip2 install -U requests

# youtube_dl good tbh
do_root pip3 install -U aiohttp youtube_dl

# tools
do_pkg wine-staging winetricks
do_pkg qutebrowser firefox
do_pkg redshift scrot
do_root pip3 install Glances

# ntp that isnt systemd is good
if [ $1 = "laptop" ]; then
    do_pkg chrony
else
    do_pkg ntp
fi

# install audio stuff
# void linux needs ConsoleKit2 for some reason
# no idea what it is but im going with it
do_pp ConsoleKit2 ""
do_pkg alsa-utils pulseaudio pavucontrol

if [ $PKG = "xbps-install" ]; then
    sudo ln -s /etc/sv/alsa /var/service/
    sudo ln -s /etc/sv/dbus /var/service/
    sudo ln -s /etc/sv/cgmanager /var/service/
    sudo ln -s /etc/sv/consolekit /var/service/
fi

# utils
do_pkg nethogs ncdu mc
do_pkg ntfs-3g udevil pcmanfm spacefm

# libraries
do_pkg mono mono-devel

# good shit
do_pkg aria2 cloc cmake cmatrix conky pydf

# utils
do_pkg hddtemp htop lynx mosh most multitail mtr curl nc tar zip maim slop

# media
do_pkg mpv mplayer smplayer x264 ffmpeg mate-icon-theme

# fonts
do_pkg ttf-dejavu ttf-liberation terminus-font

echo "dab!"
