#!/usr/bin/fish

if status --is-interactive
    # eval sh $HOME/.config/base16-shell/scripts/base16-black-metal-dark-funeral.sh
end

set -gx EDITOR "nvim"
set -gx VISUAL "nvim"

set PATH $HOME/bin $PATH
set PATH $HOME/.local/bin $PATH
set PATH $HOME/til/bin $PATH

# project-specific paths
set PATH $HOME/git/hux/src $PATH
set PATH $HOME/.yarn/bin $PATH

# lang-specific
set PATH $HOME/go/bin $PATH
set PATH $HOME/.cargo/bin $PATH
set PATH $HOME/.npm-global/bin $PATH
set PATH $HOME/.gem/ruby/2.5.0/bin $PATH
set PATH $HOME/.luarocks/bin $PATH

alias moonbkp="duplicity /home/luna file:///run/media/luna/KITTY_PALACE/moonbackup --progress --exclude \"**cache/**\" --exclude \"/home/luna/.local/share/Steam\" --exclude \"/home/luna/Virtualbox VMs/\" --exclude \"/home/luna/.cache\" --exclude \"/home/luna/.wine\" --exclude \"/home/luna/.thumbnails\""

alias moonbkp_borg="borg create --stats --progress --compression lz4 --exclude \"*cache/*\" --exclude \"*.local/share/Steam\" --exclude \"*/Virtualbox VMs/\" --exclude \"*/.cache\" --exclude \"*/.wine\" --exclude \"*/.thumbnails\" --exclude \"*/.local/share/Trash\" --exclude \"*.config/itch\" '::{user}-{now}' ~"

alias p="ps aux | grep"
alias c+x="chmod +x -v"

# youtube-dl aliases
alias ytdl="youtube-dl --socket-timeout 2"
alias ytdl360="ytdl -f 18"

# git aliases
alias gtst='git status'
alias gaa='git add -A'
alias gct='git commit'
alias gcta='git commit -a'
alias gpsh='git push'
alias gpll='git pull'
alias gact='git add -A && git commit'
alias gpllad='find . -mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} pull origin master \;'
alias gpshad='find . -mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} push origin master \;'

alias git_cur_branch='git branch | grep \* | cut -d \' \' -f2'

# batch convert from GIF to APNG
# stage 1: compress the gifs
alias batapng_s1="find . -type f -iname '*.gif' | sed 's/\.gif\$//1' | xargs -I '{}' gifsicle '{}.gif' -O2 --colors 80 --resize 64x64 -o '{}.tmp'"

# stage 2: convert from gif to apng
alias batapng_s2="find . -type f -iname '*.tmp' | sed 's/\.tmp\$//1' | xargs -I '{}' ffmpeg -i '{}.tmp' -f apng -plays 0 '{}.png'"

alias batapng="batapng_s1; batapng_s2"

set -gx BORG_REPO "/media/KITTY_PALACE/repo"

set -gx DOTNET_ROOT "$HOME/dotnet"

function fish_right_prompt
  # intentionally left blank
end

# adb backup functions
function adb_backup
  mkdir -p ~/phone_backup/full

  set -l bkp_date (date +'%Y-%m-%d-%H_%M_%S')
  set -l bkp_path (realpath ~/phone_backup/full/$bkp_date.adb)

  adb backup -f $bkp_path -apk -obb -shared -all -nosystem
end

# snapshots only contain apk, obb, not shared storage.
function adb_snapshot
  mkdir -p ~/phone_backup/snapshot
  set -l bkp_date (date +'%Y-%m-%d-%H_%M_%S')
  set -l bkp_path (realpath ~/phone_backup/snapshot/$bkp_date.adb)

  adb backup -f $bkp_path -apk -obb -noshared -all -nosystem
end

. $HOME/til/fish/colors.fish
