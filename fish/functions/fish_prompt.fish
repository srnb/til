# fish_git_prompt configuration, taken of kawasaki theme
set __fish_git_prompt_color_merging              red
set __fish_git_prompt_color_branch               brblue
set __fish_git_prompt_showcolorhints             yes
set __fish_git_prompt_show_informative_status    yes
set __fish_git_prompt_char_stateseparator        ' '

function __my_prompt_git
    # __fish_git_prompt is available, but fish_git_prompt is not, for some
    # unknown reason, so I have to use it
    #
    # the sed call was also taken off kawasaki, my modifications were
    # simplification of code by using set_color
    set git_prompt (__fish_git_prompt | command sed -e 's/^ (//' -e 's/)$//')
    echo $git_prompt
end

function fish_prompt --description 'Write out the prompt (luna style)'
    # the default changes depending if root or not, but
    # those configs are linked to ~/.config/fish for my user, not
    # root, so i can just hardcode those as they are. extract milliseconds,
    # as they say..
    set color_cwd $fish_color_cwd
    set suffix '$'

    # split the main prompt into its components
    set userhost "$USER@"(prompt_hostname)

    set cwd ""(set_color $color_cwd)(prompt_pwd)
    set git (__my_prompt_git)
    set suffix (set_color normal)'$ '

    # join them together
    printf "%s:%s %s\n%s" $userhost $cwd $git $suffix 
end
