#!/bin/bash
#
# SCRIPT: sys-vitalbackup-Linux.sh
# AUTHOR: Janos Gyerik <info@janosgyerik.com>
# CONTRI: Luna Mendes <luna@l4.pm>
# PLATFORM: Arch Linux / Void Linux
#
# PURPOSE: This program takes a backup of important statistics, configuration
#          files and other files.
#
# set -n   # Uncomment to check your syntax, without execution.
#          # NOTE: Do not forget to put the comment back in or
#          #       the shell script will not execute!
# set -x   # Uncomment to debug this shell script (Korn shell only)
#

usage() {
    test $# = 0 || echo $@
    echo "Usage: $0 [OPTION]..."
    echo
    echo Backup most vital system files and most relevant system information
    echo
    echo "  -f, --file FILE       Save tarball in FILE"
    echo "      --light           Save less information, default = $light"
    echo
    echo "  -h, --help            Print this help"
    echo
    exit 1
}

#flag=off
#param=
light=off
file=
while [ $# != 0 ]; do
    case $1 in
    -h|--help) usage ;;
#    -f|--flag) flag=on ;;
#    -p|--param) shift; param=$1 ;;
    --light) light=on ;;
    -f|--file) shift; file=$1 ;;
#    --) shift; while [ $# != 0 ]; do args="$args \"$1\""; shift; done; break ;;
    -?*) usage "Unknown option: $1" ;;
#    *) args="$args \"$1\"" ;;  # script that takes multiple arguments
#    *) test "$arg" && usage || arg=$1 ;;  # strict with excess arguments
    *) usage ;;
    esac
    shift
done

OSNAME=$(uname -s)
HOSTNAME=$(hostname)
VERSION=$(uname -r)
DATE=$(date -u "+%F")
tmpdir=$OSNAME-$HOSTNAME-$VERSION-$DATE
test $light = on && tmpdir=$tmpdir-light
test "$file" || file=$tmpdir.tar.gz

trap 'rm -fr $file $tmpdir; exit 1' 1 2 3 15

mkdir -p $tmpdir

if [ $light = off ]; then
    echo "* saving /etc preserving permissions ..."
    tar cf - /etc | tar xpf - -C $tmpdir
    echo "* saving /boot ..."
    cp -RL /boot $tmpdir/boot

    echo "* saving MBR (without partition table)..."
    dd if=/dev/sda of=$tmpdir/mbr-backup-nopt bs=446 count=1

    echo "* saving MBR (with partition table)..."
    dd if=/dev/sda of=$tmpdir/mbr-backup-pt bs=512 count=1

    echo "* saving package manager list"
    which pacman >/dev/null 2>&1
    if [ $? -eq 1 ]
    then
        echo -e " * pacman not found, trying xbps\n"
        mkdir $tmpdir/xbps

        xbps-query -l > $tmpdir/xbps/installed
    else
        echo -e " * using pacman"
        mkdir $tmpdir/pacman

        pacman -Qqen > $tmpdir/pacman/pkglist.txt
        cp /etc/pacman.d/mirrorlist $tmpdir/pacman/mirrorlist
    fi

else
    echo "* saving _some_ files in /etc while preserving permissions ..."
    tar cf - $(du -sk /etc/* | awk '$1 < 300 {print $2}') /etc/init.d | tar xpf - -C $tmpdir
fi

echo "* saving /proc info ..."
w=$tmpdir/proc
mkdir $w
for i in cmdline cpufreq cpuinfo crypto devices diskstats dma execdomains fb filesystems interrupts iomem ioports loadavg locks mdstat meminfo misc modules mounts mtrr partitions stat swaps sysrq-trigger uptime version vmstat; do
    test -f /proc/$i && cat /proc/$i > $w/$i
done

echo "* saving the output of some commands ..."
w=$tmpdir/out
mkdir $w
dmesg > $w/dmesg
cp /var/log/dmesg $w/var-log-dmesg
lspci -v > $w/lspci-v
lsmod > $w/lsmod
lsusb > $w/lsusb
iptables -L > $w/iptables-L
ss -na > $w/ss-na

echo "* tgz-ing it all up in $file ..."
tar cf - $tmpdir | gzip -c > $file

echo "* cleaning up ..."
rm -fr $tmpdir

echo "* done."
