#!/usr/bin/python3.6
# notmuch is required.

import re
import subprocess
import email
import time
import logging
from pathlib import Path

from notmuch import Query, Database

MAILDIR = '/home/luna/Mail/mailbox'
MAIL_ICON_PATH = '/usr/share/icons/mate/48x48/status/mail-unread.png'
log = logging.getLogger(__name__)


def notify_err(msg):
    subprocess.run(
        ['notify-send', '-i', MAIL_ICON_PATH, '-u', 'critical', msg])


def notify(msg):
    subprocess.run(
        ['notify-send', '-i', MAIL_ICON_PATH, msg])


def setup_logging():
    logging.basicConfig(
        level=logging.INFO, filename='/home/luna/.cache/mailmain.log')


def main():
    proc = subprocess.run(
        ['notmuch new'], shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        encoding='utf-8')

    if proc.returncode != 0:
        notify_err('failed to run notmuch')
        return

    ending_line = proc.stdout.split('\n')[-2]
    ending_line = ending_line.lower()

    # gotta make sure to account for edge-cases or smth
    # since i'm not using the notmuch bindings here
    if ending_line.startswith('no new mail'):
        print('no new mail, stopping.')
        return

    # we got mail!. what do...
    m = re.search(r'(\d+)', ending_line)
    new_msgs = m.group(0)
    print('there are', new_msgs, 'new messages')

    mail = Database('/home/luna/Mail/mailbox/')
    query = Query(mail, 'tag:inbox and date:yesterday..today')
    msgs = query.search_messages()

    try:
        latest = next(iter(msgs))
    except StopIteration:
        notify_err(f'notmuch sent {new_msgs}, found none')
        return

    with open(latest.get_filename()) as fp:
        message = email.message_from_file(fp)

    from_addr = message['from']
    subject = message['subject']

    notify_line = (
        f'mail: new {new_msgs} messages\n'
        f'{subject}\nby {from_addr}'
    )

    print(notify_line)
    notify(notify_line)


if __name__ == '__main__':
    setup_logging()

    try:
        log.info('starting main %f', time.time())
        main()
    except:
        log.exception('failed to run main')
