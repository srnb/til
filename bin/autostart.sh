#!/usr/bin/env bash
# autostart.sh - start my shit when i wake up
#  made because this laptop can't suspend or hibernate for shit

# thanks https://wiki.archlinux.org/index.php/awesome#Autorun_programs

function run {
  if ! pgrep $1 ;
  then
    notify-send "firing up $1"
    $@&
  fi
}

notify-send "autostart!"

# for some reason the layout for the keyboard keeps resetting when
# starting the system, or when unplugging it, probably x11 being
# a piece of shit. ensure we have a 'br' layout
setxkbmap -layout br

# redshift is important for quality of life
run redshift

# 1 (one) terminal
run st
run gnome-system-monitor
run conky
run syncthing-gtk

# epic meme sending
run flameshot

# run xscreensaver -nosplash
