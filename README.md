# til

The dotfiles(Mostly `fish` and `awesomewm`).

## Install

### "Dependencies"

this has configurations for:
 - fish (with oh-my-fish), bash
 - awesomewm
 - st, urxvt, kitty
 - vim, neovim
 - emacs
 - tmux
 - redshift
 - kitty
 - mpd (unmaintained)
 - conky
 - qutebrowser (for userscripts)

**NOTE: everything here is tailored for my setup, so
all scripts are hardcoded (assume my user, `luna`,
and my home directory, `/home/luna`, there may be more).**

```bash
cd ~

git clone https://gitlab.com/luna/til.git

cd til

# link config files to their respective targets
./helper.bash

omf reload
```

## `/bin` ?

shit made by me (or not), for me, goes from
uploading screenshots to ipfs to making a backup of /etc. don't think
they'd be usable by anyone else, but still.

## `init.bash` ?

basically install some necessary packages for sanity of mind,
run `./init.bash laptop` if you like chrony.

## `init.fish`?

fish is my main shell, there's the config for it.

## what about a missing `nvimrc.secret`?

that's for code::stats, do something there.

## do you use vim or emacs

vim and neovim. emacs configs are for historical purposes.
