set termguicolors

call plug#begin('~/.local/share/nvim/plugged')

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

Plug 'w0rp/ale'

" language support
Plug 'fatih/vim-go'
Plug 'arrufat/vala.vim'
Plug 'rhysd/vim-crystal'
Plug 'dag/vim-fish'
Plug 'ziglang/zig.vim'

Plug 'elixir-editors/vim-elixir'
Plug 'slashmili/alchemist.vim'

" themes
" Plug 'nanotech/jellybeans.vim'
Plug 'tssm/fairyfloss.vim'
Plug 'dracula/vim', { 'as': 'dracula' }

" my old vim shit
Plug 'https://gitdab.com/lavatech/vim-rana.git'
Plug 'https://gitlab.com/code-stats/code-stats-vim.git'
Plug 'vim-airline/vim-airline'

" misc
Plug 'rhysd/committia.vim'
Plug 'vim-scripts/dbext.vim'
Plug 'editorconfig/editorconfig-vim'

call plug#end()

source /home/luna/til/vim/base.vim

" Put all temporary files under the same directory.
" https://github.com/mhinz/vim-galore#handling-backup-swap-undo-and-viminfo-files
set backup
set backupdir   =$HOME/.nvim/files/backup/
set backupext   =-vimbackup
set backupskip  =
set directory   =$HOME/.nvim/files/swap/
set updatecount =100
set undofile
set undodir     =$HOME/.nvim/files/undo/
set viminfo     ='100,n$HOME/.nvim/files/info/viminfo

colorscheme dracula

let g:ale_python_auto_pipenv = 1
let g:ale_python_mypy_ignore_invalid_syntax = 1
let g:ale_fixers = {
            \    'python': ['yapf']
            \}
let g:ale_linters = {
            \   'elixir': ['credo']
            \}

let g:crystal_auto_format = 1

" only for nvim, has the code-stats-vim token
source /home/luna/til/nvimrc.secret
