;;; Code
(require 'package)

(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(setq package-enable-at-startup nil)
(package-initialize)

;; because I hate ~ files being commited
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)

(setq delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)

;; setup tabs
(setq-default indent-tabs-mode nil)

;; keep both of those 4 pls
(setq-default tab-width 4)
(setq-default c-basic-offset 4)

;; ui tweaks

(when (display-graphic-p)
  (toggle-scroll-bar -1))

(tool-bar-mode -1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("5d8c90053cf9c111aa50d26596a746fc0dacd18f6bc75fba8251fdd5f43a6277" "ff7625ad8aa2615eae96d6b4469fcc7d3d20b2e1ebc63b761a349bebbb9d23cb" default)))
 '(package-selected-packages
   (quote
    (tabbar lua-mode flycheck exec-path-from-shell magit dracula-theme alchemist alchemist\.el wakatime-mode evil-mode use-package ## evil-visual-mark-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(use-package evil
  :ensure t
  :config (evil-mode 1))

(use-package wakatime-mode
  :ensure t)

(use-package alchemist
  :ensure t)

(use-package magit
  :ensure t)

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package flycheck-mix
  :ensure t)

(use-package lua-mode
  :ensure t)

(use-package elcord
  :ensure t)

(use-package tabbar
  :ensure t)

;; package configuration
(setq wakatime-cli-path "/usr/bin/wakatime")
(require 'wakatime-mode)
(global-wakatime-mode)

(add-to-list 'custom-theme-load-path "~/til/emacs/themes/")
(load-theme 'fairyfloss t)

(global-set-key (kbd "C-x g") 'magit-status)

(require 'flycheck-mix)
(flycheck-mix-setup)
(elcord-mode)
(tabbar-mode)

(set-face-attribute 'default nil :height 120)
(set-frame-font "PragmataPro")

(provide '.emacs)
;;; .emacs ends here
